from django.test import TestCase, Client
from django.urls import reverse, resolve

class TestStory7(TestCase):
    def test_url_ada_apa_nggak(self):
        response = Client().get('')
        self.assertEquals(200, response.status_code)

    def test_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'main/story7.html')

    
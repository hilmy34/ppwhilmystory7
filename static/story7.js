$( function() {
    $( "#accordion" ).accordion({
      collapsible: true,
      active: false,
      heightStyle: "content"
    });      
  } );

$(".arrowUp").click(function(){
  let headSekarang = $(this).parent().parent();
  let isiSekarang = headSekarang.next();
  let isiSebelum = headSekarang.prev();
  let headSebelum = isiSebelum.prev();

  isiSekarang.insertBefore(headSebelum);
  headSekarang.insertBefore(isiSekarang);
})

$(".arrowDown").click(function(){
  let headSekarang = $(this).parent().parent();
  let isiSekarang = headSekarang.next();
  let headSesudah = isiSekarang.next();
  let isiSesudah = headSesudah.next()

  isiSekarang.insertAfter(isiSesudah);
  headSekarang.insertBefore(isiSekarang);
})

$(document).on('click', ".arrow", function(){   
  $(".isi").each(function() {
		$(this).removeClass("ui-accordion-content-active");
		$(this).css("display","none");
	})

	$(".head").each(function() {
		$(this).removeClass("ui-accordion-header-active ui-state-active ui-state-focus");
		$(this).addClass("ui-accordion-header-collapsed ui-corner-all");
	})

	$(".ui-icon").each(function() {
		$(this).removeClass("ui-icon-triangle-1-s");
		$(this).addClass("ui-icon-triangle-1-e");
	})

})

